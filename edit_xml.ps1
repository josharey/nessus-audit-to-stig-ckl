function Get-OpenFindingDetails{
    Param(
        [Parameter(Mandatory=$true)]
        [string]$hostIP,

        [Parameter(Mandatory=$true)]
        [System.Xml.XmlDocument]$nessusXML,

        [Parameter(Mandatory=$false)]
        [PSDefaultValue(Help = 'Nessus Status to find. FAILED, PASSED, WARNING')]
        [ValidateSet("FAILED","WARNING","ERROR","PASSED")]
        [string[]]$statusToFind = @("FAILED","PASSED")
    )

    # custom class to return a more abreviated object
    class Finding {
        [string]$vkey
        [string]$status
        [string]$details
    }

    # namespace for xpath queries
    $ns = @{cm="http://www.nessus.org/cm"}

    # extract the nessus "report" section for just the host IP from the checklist
    $hostreport = $nessusxml.SelectSingleNode("//ReportHost[./HostProperties/tag[@name='host-ip'] = '$hostip']")

    # expand status if more than one given as a parameter
    $statuses = [string]::join(",",$statusToFind)

    # extract just the policies with the desired status, defaults to FAILED (each in their own "reportitem") from the nessus file
    $reportitems = Select-Xml -xml $hostreport -XPath "//ReportItem[./cm:compliance-result[contains('$statuses',text())]]" -Namespace $ns

    # initialize array to store values from findings since we will reuse the var later
    $findings = @()

    # extract the v-key and useful finding details, then store those in the $findings hashtable
    foreach($reportitem in $reportitems){
        # (re)initialize object to hold results to append to the $findigs array later
        $thisfinding = [Finding]::new()
        
        # have to slice out just the vkey from the long reference string but some. also some warnings don't have v-keys associated
        if($null -ne $reportitem.node.'compliance-reference' -and -not $reportitem.node.'compliance-policy-value'.contains("manual review")){
            # get just the vkey out of the reference
            try {$thisfinding.vkey = $reportitem.node.'compliance-reference'.Split("|")[-1].trim()}
            catch {"could not set vkey for $thisfinding.vkey"}
            
            # extract both required and actual values
            try {$thisfinding.details = "Policy Value:`n" + $reportitem.node.'compliance-policy-value' + "`n`nActual Value:`n" + $reportitem.node.'compliance-actual-value'}
            catch {"could not set details for $thisfinding.vkey"}
            
            # add the status. Anything not a "PASSED" will be open
            # TO-DO: account for NA
            if ($reportitem.node.'compliance-result' -eq "PASSED"){
                try {$thisfinding.status = "NotAFinding"}
                catch {"could not set status for $thisfinding.vkey"}
            } else {
                try {$thisfinding.status = "Open"}
                catch {"could not set status for $thisfinding.vkey"}
            }
        }
        # append the vkey and details to the findings list if that vkey has not already in findings
        # To-Do: are we missing valid finding by ignoring duplicate vkeys?
        if ($thisfinding.vkey -notin $findings.vkey){
            $findings += $thisfinding
        }
    }
    # return the array of custom object [Finding]
    return $findings
}

function Update-Checklist{
    Param(
        # checklist filenames need to be absolute paths (ie: fullname)
        # gci -recurse -include *string.ext | select -expandproperty fullname
        [Parameter(Mandatory=$true,ParameterSetName = "nessus")]
        [Parameter(Mandatory=$true,ParameterSetName = "force")]
        [string[]]$ChecklistFiles,

        [Parameter(Mandatory=$true,ParameterSetName = "nessus")]
        [string]$NessusFile,

        [Parameter(Mandatory=$true,ParameterSetName = "force")]
        [switch]$ForceUpdate,

        [Parameter(Mandatory=$true,ParameterSetName = "force")]
        [string]$VKeyToUpdate,

        [Parameter(Mandatory=$true,ParameterSetName = "force")]
        [ValidateSet("Open","NotAFinding","Not_Applicable")]
        [string]$StatusToSet,

        [Parameter(Mandatory=$false,ParameterSetName = "force")]
        [string]$FindingDetailsToSet,

        [Parameter(Mandatory=$false,ParameterSetName = "nessus")]
        [string]$FileNameAppendString
    )

    if ($NessusFile){
        # generate xmldocument from nessus file
        [xml]$nessusXML = Get-Content $NessusFile
    }

    # iterate over all the provided checklists
    foreach ($checklist in $checklistFiles){
        # generate xmldocument from ckl file. This is needed to allow preservewhitespace to prevent invalid XML during save
        $checklistXML = New-Object System.Xml.XmlDocument
        $checklistXML.PreserveWhitespace = $true
        $checklistXML.Load($checklist)

        # $forceupdate parameter set
        if ($ForceUpdate){
            # update status 
            try {
                # get xml node to and if it's vkey matches the vkeytoupdate, set the new status
                $node = $checklistxml.SelectSingleNode("//VULN[./STIG_DATA/ATTRIBUTE_DATA[text()='$VKeyToUpdate']]/STATUS")
                if ($node){
                    $node."#text" = $StatusToSet
                }
            }
            catch {
                "Could not update finding status for $VKeyToUpdate in file $checklist"
            }

            if ($FindingDetailsToSet){
                #update finding details
                try{
                    # get finding_status node similar to above
                    $node = $checklistxml.SelectSingleNode("//VULN[./STIG_DATA/ATTRIBUTE_DATA[text()='$VKeyToUpdate']]/FINDING_DETAILS")
                    if ($node){
                        $node."#text" = $FindingDetailsToSet
                    }
                } 
                catch {
                    "Could not update finding details for $VKeyToUpdate in file $checklist"
                }
            }
        }

        # nessus parameter set
        else {
            # extract hostIP from checklist. fail if IP address is empty
            $hostIP = $checklistXML.SelectSingleNode("//HOST_IP").'#text'

            # make sure an IP was set in the file. if not, exit
            if ($hostIP -eq "" -or $null -eq $hostIP){return "$checklist has no IP set in checklist"}

            # call function to get nessus finding for this host
            $findings = Get-OpenFindingDetails -hostIP $hostIP -nessusXML $nessusXML

            # get keys in checklist file. get all elements that have the text 'Vuln_Num' and get the next attribute which is always the vkey in a STIG checklist
            $checklistkeys = $checklistXML.SelectNodes("//*[text() = 'Vuln_Num']/following-sibling::ATTRIBUTE_DATA").'#text'

            # iterate over findings and update the base checklist with nessus findings
            foreach($finding in $findings){
                # only update checklist if the open keys match. otherwise if there is more than one checklist (i.e. windows and firewall and .net) all the checklists will populate with all the findings for that host in aggregate
                if ($finding.vkey -in $checklistkeys){

                    # update finding details. need to pass a string instead of an object though
                    $thisVkey = $finding.vkey
                    
                    #update finding details
                    try{
                        $checklistxml.SelectSingleNode("//VULN[./STIG_DATA/ATTRIBUTE_DATA[text()='$thisVkey']]/FINDING_DETAILS").InnerText += $finding.details
                    } 
                    catch {
                        "Could not update finding details for $thisVkey in file $checklist"
                    }

                    # update status 
                    try {
                        $checklistxml.SelectSingleNode("//VULN[./STIG_DATA/ATTRIBUTE_DATA[text()='$thisVkey']]/STATUS").InnerText = $finding.status
                    }
                    catch {
                        "Could not update finding status for $thisVkey in file $checklist"
                    }
                }
            }
        }
        # save the file out. update filename if selected
        $outfilename = $checklist.trim(".ckl") + $FileNameAppendString + ".ckl"
        $checklistXML.save($outfilename)
    }
}